# php-twitter-client

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Software License][ico-license]](LICENSE.md)
[![Build Status][ico-gitlab-build]][link-gitlab-build]
[![Pipeline status][ico-gitlab-pipeline]][link-gitlab-pipeline]
[![Coverage report][ico-gitlab-cov]][link-gitlab-cov]
[![Total Downloads][ico-downloads]][link-downloads]
<!-- [![Coverage Status][ico-scrutinizer]][link-scrutinizer] -->
<!-- [![Quality Score][ico-code-quality]][link-code-quality] -->

This is where your description should go. Try and limit it to a paragraph or two, and maybe throw in a mention of what PSRs you support to avoid any confusion with users and contributors.

## Structure

If any of the following are applicable to your project, then the directory structure should follow industry best practices by being named the following.

```
bin/        
build/
docs/
config/
src/
tests/
vendor/
```


## Install

Via Composer

``` bash
$ composer require narad1972/php-twitter-client
```

## Usage

``` php
$skeleton = new League\Skeleton();
echo $skeleton->echoPhrase('Hello, League!');
```

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Testing

``` bash
$ composer test
```

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) and [CODE_OF_CONDUCT](CODE_OF_CONDUCT.md) for details.

## Security

If you discover any security related issues, please email narad1972@gmail.com instead of using the issue tracker.

## Credits

- [Nir Arad][link-author]
- [All Contributors][link-contributors]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

<!-- Packagist -->
[ico-version]: https://img.shields.io/packagist/v/narad1972/php-twitter-client.svg?style=flat-square
[link-packagist]: https://packagist.org/packages/narad1972/php-twitter-client

<!-- License -->
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square

<!-- Build -->
[ico-gitlab-build]: https://img.shields.io/gitlab/pipeline/narad1972/php-twitter-client/master
[link-gitlab-build]: https://gitlab.com/narad1972/php-twitter-client/-/releases

<!-- Pipeline status -->
[ico-gitlab-pipeline]: https://gitlab.com/narad1972/php-twitter-client/badges/master/pipeline.svg
[link-gitlab-pipeline]: https://gitlab.com/narad1972/php-twitter-client/commits/master

<!-- Coverage -->
<!-- [ico-scrutinizer]: https://img.shields.io/scrutinizer/coverage/g/narad1972/php-twitter-client.svg?style=flat-square -->
<!-- [link-scrutinizer]: https://scrutinizer-ci.com/g/narad1972/php-twitter-client/code-structure -->

[ico-gitlab-cov]: https://gitlab.com/narad1972/php-twitter-client/badges/master/coverage.svg
[link-gitlab-cov]: https://gitlab.com/narad1972/php-twitter-client/-/graphsmaster/charts

<!-- Quality -->
[ico-code-quality]: https://img.shields.io/scrutinizer/g/narad1972/php-twitter-client.svg?style=flat-square
[link-code-quality]: https://scrutinizer-ci.com/g/narad1972/php-twitter-client

<!-- Downloads -->
[ico-downloads]: https://img.shields.io/packagist/dt/narad1972/php-twitter-client.svg?style=flat-square
[link-downloads]: https://packagist.org/packages/narad1972/php-twitter-client

<!-- Credits -->
[link-author]: https://gitlab.com/narad1972
[link-contributors]: https://gitlab.com/narad1972/php-twitter-client/-/graphs/master
