<?php

namespace narad1972\TwitterClient;

class FieldTypes {
    const FIELD_INT = 0;
    const FIELD_STRING = 1;
    const FIELD_DATE = 2;
    const FIELD_ENUM = 3;
    const FIELD_ARRAY = 4;
}

?>